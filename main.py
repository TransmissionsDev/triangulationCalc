import math
import inquirer
from time import sleep
import numpy as np
from numpy import pi, sin, cos
import matplotlib.pyplot as plt


print('Welcome to the Triangulation Calculator')
print('Basically, we will generate a (text based) 2d map')


print('We are going to need some information. We will generate a map based on the information given')
print("First, let's make a map based on the information you give me.")
print('Which unit will you be using')

answers = input()


print('Ok, now we are going to need you to type in some information')
print('How large to you want the x and y lines to be for your graph')
thelength = input()

print('So your graph will now be ' + thelength + ' by ' + thelength + '!')

print('Now I need three locations on the map.')
print('The first point please! Start with the x then y coordinates!')
p1x = input()
p1y = input()
p1x = int(p1x)
p1y = int(p1y)

print("Nice! What is the next point's coordinates?")
p2x = input()
p2y = input()
p2x = int(p2x)
p2y = int(p2y)

print('What are the coordinates of the next point?')
p3x = input()
p3y = input()
p3x = int(p3x)
p3y = int(p3y)

xDis1 = int(p1x) - int(p2x)

if xDis1 < 0:
    xDis1 = int(xDis1) * -1
yDis1 = p1y - p2x
if yDis1 < 0:
    yDis1 = int(yDis1) * -1

xDis1 = int(xDis1) * int(xDis1)
yDis1 = int(yDis1) * int(yDis1)

nearFinal = int(xDis1) + int(yDis1)

final = math.sqrt(nearFinal)

thefinal = round(int(final))
print('The distance between your first and second point is ')
print(str(thefinal) + ' ' + answers + '! ' + 'Without rounding, we got ' + str(final) + ' ' + answers)
print(' ')
print(' ')

xDis2 = int(p1x) - int(p3x)
if xDis2 < 0:
    xDis2 = xDis2 * -1
yDis2 = int(p1y) - int(p3y)
if yDis2 < 0:
    yDis2 = yDis2 * -1

xDis2 = xDis2 * xDis2
yDis2 = yDis2 * yDis2

nearFinal = xDis2 + yDis2

final2 = math.sqrt(nearFinal)

theFinal2 = str(round(final2))

print('The distance between your first and third points')
print('is around ' + theFinal2 + answers + ', but exactly '+ str(final2) + ' ' + answers)
print(' ')
print(' ')

#NZXT
xDis3 = int(p2x) - int(p3x)
if xDis3 < 0:
    xDis3 = xDis3 * -1
yDis3 = int(p2y) - int(p3y)
if yDis3 < 0:
    yDis3 = yDis3 * -1

xDis3 = xDis3 * xDis3
yDis3 = yDis3 * yDis3

nearFinal = xDis3 + yDis3

final3 = math.sqrt(nearFinal)

theFinal3 = str(round(final3))

print('The distance between your second and third points')
print('is around ' + theFinal3 + answers + ', but exactly '+ str(final3) + ' ' + answers)
print(' ')
print(' ')

sleep(10)

print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')
print(' ')



sleep(1)
print('What is the EXACT distance from point A to your specific area?')
print('This will serve as the radius of othe circle we will develop')
p1r = input()
print('What is the EXACT distance from point B to the unknown point')
p2r = input()

print('What is th EXACT distance from point B to unknown point?')
p3r = input()



#this is the program that does stuff

p1Circ = []
p2Circ = []
p3Circ = []

def p1CircleGen():
    global p1Circ
    global p1x
    global p1y
    global p1r

    r = float(p1r)
    radians = np.linspace(0, 2*pi, 100)

    xvalues = r*cos(radians)
    yvalues = r*sin(radians)
    coordinates = [[xvalue + p1x, yvalue + p1y]

    for xvalue, yvalue in zip(xvalues, yvalues)]

    fig, ax = plt.subplots(figsize=(2,2))
    ax.scatter(xvalues,yvalues)

    p1Circ.append(coordinates)
p1CircleGen()

def p2CircleGen():
    global p2Circ
    global p2x
    global p2y
    global p2r

    r = p2r
    radians = np.linspace(0, 2*pi, 100)

    xvalues = r*cos(radians)
    yvalues = r*sin(radians)
    coordinates = [[xvalue + p2x, yvalue + p2y]
    for xvalue, yvalue in zip(xvalues, yvalues)]

    fig, ax = plt.subplots(figsize=(2,2))
    ax.scatter(xvalues,yvalues)
    p2Circ.append(coordinates)

def p3CircleGen():
    global p3Circ
    global p3x
    global p3y
    global p3r


    r = p3r
    radians = np.linspace(0, 2*pi, 100)

    xvalues = r*cos(radians)
    yvalues = r*sin(radians)
    coordinates = [[xvalue + p3x, yvalue + p3y]
    for xvalue, yvalue in zip(xvalues, yvalues)]

    fig, ax = plt.subplots(figsize=(2,2))
    ax.scatter(xvalues,yvalues)
    p3Circ.append(coordinates)
xxx = len(p1Circ) - 1

roundp11 = []
roundp12 = []
roundp13 = []
roundp14 = []
roundp15 = []
roundp16 = []
roundp17 = []
roundp18 = []
roundp19 = []
roundp110 = []
roundp111 = []
roundp112 = []
roundp113 = []
roundp114 = []
roundp115 = []

for i in range(0,xxx):
    x = p1Circ[i]

    roundp11.append(round(x,1))
    roundp12.append(round(x,2))
    roundp13.append(round(x,3))
    roundp14.append(round(x,4))
    roundp15.append(round(x,5))
    roundp16.append(round(x,6))
    roundp17.append(round(x,8))
    roundp19.append(round(x,9))
    roundp111.append(round(x,11))
    roundp112.append(round(x,12))
    roundp113.append(round(x,13))
    roundp114.append(round(x,14))











xxx = len(p3Circ) - 1
found = []
for ting in range(0,xxx):
    if p1Circ[ting] in p2Circ:
        found.append(p1Circ[ting])
        xxx = p3Circ - 1
        for  Hang in range(0, xxx):
            if p3Circ[Hang] in found:
                print('We have found the location!')
                print(p3Circ[Hang] + ' is the location!')





















