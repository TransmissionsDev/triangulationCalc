# triangulationCalc

triangulationCalc is a calculator that is able to find the GPS coordinates of a specific location using Triangulation!

The definition of Triangulation is:
(in surveying) the tracing and measurement of a series or network of triangles in order to determine the distances and relative positions of points spread over a territory or region, especially by measuring the length of one side of each triangle and deducing its angles and the length of the other two sides by observation from this baseline.

This essentially means that you can use 3 points on a map (which always generates a triangle) to find a location. Once you calculate the distance from your specific point to each one of the triangle, you can generate a distinct circle for each point on the triangle (the distance is the radius)! Finally, your specific point is the area where all three circles line up.

# Download

For many people, downloading a python program and running it in your terminal is quite difficult. If you do not already know how to do that, here is a quick tutorial for Mac and Linux users.

Go to your terminal and type in:
> git clone https://github.com/jetiscool/triangulationCalc.git

In the scenario that this does not work, go to the home page of this repository, hover your mouse over that green thing and click download as zip.

Next in your terminal type in
> cd triangulationCalc

This puts your terminal into the file that you just downloaded. If this does not work, try to extract the .zip file you downloaded. Otherwise type in:
> python3 main.py

And boom! The program starts!
